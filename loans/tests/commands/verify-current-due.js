var Assert = require('../../lib/assert.js');

module.exports = function(spec, context) {
  return function() {
    var date = spec.date;
    var expectedAmount = parseFloat(spec.expectedCurrentDue);
    console.log('current due on %s should be %s', date, expectedAmount);
    return context.api.loan.getCurrentDue(context.refNumber, date).then(function(currentDue) {
      Assert.equal(expectedAmount, currentDue);
    });
  };
};
