var abort = require('../../lib/abort.js');
var csvToJson = require('../../lib/csvtojson.js')
var math = require('mathjs');
var path = require('path');
var util = require('util');
var Assert = require('../../lib/assert.js');
var StringFormat = require('../../lib/string-format.js');

function isNumber(v) {
  return typeof v === 'number';
}

function assertEqual(expected, actual, msg) {
  if (StringFormat.isDate(expected) && StringFormat.isString(actual)) actual = actual.substr(0, 10);
  if (StringFormat.isNumber(expected)) expected = parseFloat(expected);
  if (StringFormat.isNumber(actual)) actual = parseFloat(actual);
  if (typeof actual === 'number') actual = math.round(actual, 2);
  Assert.equal(expected, actual, msg);
}

module.exports = function(data, context) {
  var file = data['file'];

  if(!file || file.trim().length == 0)
    abort(context.commandName + ': file name not specified');

  var expectedInstallments = csvToJson(path.join('tests', 'specs', file));

  return function() {
    return context.api.schedule.getSchedule(context.refNumber).then(function(actualInstallments) {
      console.log('verify %d installments', expectedInstallments.length);

      Assert.equal(expectedInstallments.length, actualInstallments.length, 'Number of installments');
      
      expectedInstallments.forEach(function(expectedInstallment, index) {
        var actualInstallment = actualInstallments[index];

        for (var propertyName in expectedInstallment) {
          var expectedProperty = expectedInstallment[propertyName];
          var actualProperty = actualInstallment[propertyName] || actualInstallment[StringFormat.toCamelCase(propertyName)];

          assertEqual(expectedProperty, actualProperty, util.format('Installment[%d].%s', index, propertyName));
        }
      });
    });
  };
};
