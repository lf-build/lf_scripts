var abort = require('../../lib/abort.js');
var Q = require('q');

function paymentData(refNumber, spec, status) {
  return {
    loanReferenceNumber: refNumber,
    dueDate: spec.dueDate,
    amount: spec.amount,
    paymentId: spec.paymentId,
    type: spec.type,
    status: status,
    method: spec.method || "ACH"
  };
}

function sendPaymentEvent(msg, eventName, paymentStatus, spec, context) {
  var data = paymentData(context.refNumber, spec, paymentStatus);

  console.log('%s: %s %s $%s (%s/%s)',
    msg,
    data.paymentId,
    data.dueDate,
    data.amount,
    data.type,
    data.method);

  var deferred = Q.defer();

  context.api.eventhub.handle('LoanUpdated', function() {
    setTimeout(deferred.resolve, 100);
  });

  context.api.eventhub.publish(eventName, data).
    fail(function() {
      abort(arguments);
    }).
    done();

  return deferred.promise;
}

module.exports = {
  added: function paymentAdded(spec, context) {
    return function() {
      return sendPaymentEvent('payment added', 'PaymentAdded', 'Pending', spec, context);
    };
  },

  received: function paymentReceived(spec, context) {
    return function() {
      return sendPaymentEvent('payment recvd', 'PaymentReceived', 'Received', spec, context);
    };
  },

  failed: function paymentFailed(spec, context) {
    return function() {
      return sendPaymentEvent('payment faild', 'PaymentFailed', 'Failed', spec, context);
    };
  }
};
