var fs = require('fs');
var abort = require('./lib/abort.js');
var readConfig = require('./lib/read-config.js');
var EventHubApi = require('./lib/eventhub-api.js');

var args = process.argv.slice(2);
var configName = args[0];
var eventName = args[1];
var batchFile = args[2];
var interval = args[3] ? parseInt(args[3]) : 0;
var startIndex = args[4] ? parseInt(args[4]) : 0;

if (!configName || !eventName || !batchFile)
  abort('Usage event-batch <config> <event_name> <batch_file.json> [interval] [start]');

var config = readConfig(configName);
var eventHub = new EventHubApi(config);

start(batchFile);

function start(batchFile) {
  var batch = parseBatchFile(batchFile);
  var index = startIndex;

  console.log('%d events in the batch', batch.length);

  function sendNextEvent() {
    var eventData = batch[index++];
    var refnum = eventData.LoanReferenceNumber || eventData.LoanReference || eventData.ReferenceNumber;
    var id = eventData.Id;

    console.log('Sending %s %d of %d', eventName, index, batch.length, refnum ? '(loan '+refnum+')' : '', id ? '(id '+id+')' : '');

    eventHub.publish(eventName, eventData).then(function() {
      if(index < batch.length)
        interval > 0 ? setTimeout(sendNextEvent, interval) : sendNextEvent();
    });
  }

  sendNextEvent();
}

function parseBatchFile(filepath) {
  return JSON.parse(fs.readFileSync(filepath, 'utf8'));
}
