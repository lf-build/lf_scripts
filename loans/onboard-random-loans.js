var Q = require('q');
var LoanApi = require('./lib/loan-api.js');
var CommandQueue = require('./lib/command-queue.js');
var abort = require('./lib/abort.js');
var readConfig = require('./lib/read-config.js');
var loadJson = require('./lib/load-json.js');

var args = process.argv.slice(2);
var configName = args[0];
var datasetFile = args[1];
var howMany = parseInt(args[2] || 0);
var interval = parseInt(args[3] || 1000);

if (!configName || howMany <= 0)
  abort('Usage onboard-random-loans <config> <dataset> <howMany> [interval]');

var config = readConfig(configName);
var dataset = loadJson(datasetFile);
var loan = new LoanApi(config);

start();

function start() {
  var queue = new CommandQueue();

  for(var index = 0; index < howMany; index++) {
    queue.add(onboardCommand());
  }

  queue.run({ interval: interval });
}

function onboardCommand() {
  return function() {
    var deferred = Q.defer();
    var refNumber = generateReferenceNumber();
    loan.onboard(refNumber, {
      "originationDate": random(dataset.originationDate),
      "loanAmount": random(dataset.loanAmount),
      "term": random(dataset.term),
      "rate": random(dataset.rate)
    }).then(function() {
      deferred.resolve();
    }).fail(function() {
      console.log('loan %s onboarding failed:', refNumber, arguments);
      deferred.resolve();
    });

    return deferred.promise;
  }
}

function random(values) {
  var index = Math.round(Math.random() * (values.length - 1));
  return values[index];
}

function generateReferenceNumber() {
  return Math.round(Math.random() * 9).toString() +
    Math.round(Math.random() * 9).toString() +
    Math.round(Math.random() * 9).toString() +
    Math.round(Math.random() * 9).toString() +
    Math.round(Math.random() * 9).toString() +
    Math.round(Math.random() * 9).toString();
}
