var Http = require('./secure-http.js');
var util = require('util');

var dataset = {
  "firstNames": ["JAMES","JOHN","ROBERT","MICHAEL","WILLIAM","DAVID","RICHARD","CHARLES","JOSEPH","THOMAS","CHRISTOPHER","DANIEL","PAUL","MARK","DONALD","GEORGE","KENNETH","STEVEN","EDWARD","BRIAN","RONALD","ANTHONY","KEVIN","JASON","MATTHEW","GARY","TIMOTHY","JOSE","LARRY","JEFFREY","FRANK","SCOTT","ERIC","STEPHEN","ANDREW","RAYMOND","GREGORY","JOSHUA","JERRY","DENNIS","WALTER","PATRICK","PETER","HAROLD","DOUGLAS","HENRY","CARL","ARTHUR","RYAN","ROGER","JOE","JUAN","JACK","ALBERT","JONATHAN","JUSTIN","TERRY","GERALD","KEITH","SAMUEL","WILLIE","RALPH","LAWRENCE","NICHOLAS","ROY","BENJAMIN","BRUCE","BRANDON","ADAM","HARRY","FRED","WAYNE","BILLY","STEVE","LOUIS","JEREMY","AARON","RANDY","HOWARD","EUGENE","CARLOS","RUSSELL","BOBBY","VICTOR","MARTIN","ERNEST","PHILLIP","TODD","JESSE","CRAIG","ALAN","SHAWN","CLARENCE","SEAN","PHILIP","CHRIS","JOHNNY","EARL","JIMMY","ANTONIO","DANNY","BRYAN","TONY","LUIS","MIKE","STANLEY","LEONARD","NATHAN","DALE","MANUEL","RODNEY","CURTIS","NORMAN","ALLEN","MARVIN","VINCENT","GLENN","JEFFERY","TRAVIS","JEFF","CHAD","JACOB","LEE","MELVIN","ALFRED","KYLE","FRANCIS","BRADLEY","JESUS","HERBERT","FREDERICK","RAY","JOEL","EDWIN","DON","EDDIE","RICKY","TROY","RANDALL","BARRY","ALEXANDER","BERNARD","MARIO","LEROY","FRANCISCO","MARCUS","MICHEAL","THEODORE","CLIFFORD","MIGUEL","OSCAR","JAY","JIM","TOM","CALVIN","ALEX","JON","RONNIE","BILL","MARY","PATRICIA","LINDA","BARBARA","ELIZABETH","JENNIFER","MARIA","SUSAN","MARGARET","DOROTHY","LISA","NANCY","KAREN","BETTY","HELEN","SANDRA","DONNA","CAROL","RUTH","SHARON","MICHELLE","LAURA","SARAH","KIMBERLY","DEBORAH","JESSICA","SHIRLEY","CYNTHIA","ANGELA","MELISSA","BRENDA","AMY","ANNA","REBECCA","VIRGINIA","KATHLEEN","PAMELA","MARTHA","DEBRA","AMANDA","STEPHANIE","CAROLYN","CHRISTINE","MARIE","JANET","CATHERINE","FRANCES","ANN","JOYCE","DIANE","ALICE","JULIE","HEATHER","TERESA","DORIS","GLORIA","EVELYN","JEAN","CHERYL","MILDRED","KATHERINE","JOAN","ASHLEY","JUDITH","ROSE","JANICE","KELLY","NICOLE","JUDY","CHRISTINA","KATHY","THERESA","BEVERLY","DENISE","TAMMY","IRENE","JANE","LORI","RACHEL","MARILYN","ANDREA","KATHRYN","LOUISE","SARA","ANNE","JACQUELINE","WANDA","BONNIE","JULIA","RUBY","LOIS","TINA","PHYLLIS","NORMA","PAULA","DIANA","ANNIE","LILLIAN","EMILY","ROBIN","PEGGY","CRYSTAL"],
  "lastNames": ["SMITH","JOHNSON","WILLIAMS","JONES","BROWN","DAVIS","MILLER","WILSON","MOORE","TAYLOR","ANDERSON","THOMAS","JACKSON","WHITE","HARRIS","MARTIN","THOMPSON","GARCIA","MARTINEZ","ROBINSON","CLARK","RODRIGUEZ","LEWIS","LEE","WALKER","HALL","ALLEN","YOUNG","HERNANDEZ","KING","WRIGHT","LOPEZ","HILL","SCOTT","GREEN","ADAMS","BAKER","GONZALEZ","NELSON","CARTER","MITCHELL","PEREZ","ROBERTS","TURNER","PHILLIPS","CAMPBELL","PARKER","EVANS","EDWARDS","COLLINS","STEWART","SANCHEZ","MORRIS","ROGERS","REED","COOK","MORGAN","BELL","MURPHY","BAILEY","RIVERA","COOPER","RICHARDSON","COX","HOWARD","WARD","TORRES","PETERSON","GRAY","RAMIREZ","JAMES","WATSON","BROOKS","KELLY","SANDERS","PRICE","BENNETT","WOOD","BARNES","ROSS","HENDERSON","COLEMAN","JENKINS","PERRY","POWELL","LONG","PATTERSON","HUGHES","FLORES","WASHINGTON","BUTLER","SIMMONS","FOSTER","GONZALES","BRYANT","ALEXANDER","RUSSELL","GRIFFIN","DIAZ","HAYES","MYERS","FORD","HAMILTON","GRAHAM","SULLIVAN","WALLACE","WOODS","COLE","WEST","JORDAN","OWENS","REYNOLDS","FISHER","ELLIS","HARRISON","GIBSON","MCDONALD","CRUZ","MARSHALL","ORTIZ","GOMEZ","MURRAY","FREEMAN","WELLS","WEBB","SIMPSON","STEVENS","TUCKER","PORTER","HUNTER","HICKS","CRAWFORD","HENRY","BOYD","MASON","MORALES","KENNEDY","WARREN","DIXON","RAMOS","REYES","BURNS","GORDON","SHAW","HOLMES","RICE","ROBERTSON","HUNT","BLACK","DANIELS","PALMER","MILLS","NICHOLS","GRANT","KNIGHT","FERGUSON","ROSE","STONE","HAWKINS","DUNN","PERKINS","HUDSON","SPENCER","GARDNER","STEPHENS","PAYNE","PIERCE","BERRY","MATTHEWS","ARNOLD","WAGNER","WILLIS","RAY","WATKINS","OLSON","CARROLL","DUNCAN","SNYDER","HART","CUNNINGHAM","BRADLEY","LANE","ANDREWS","RUIZ","HARPER","FOX","RILEY","ARMSTRONG","CARPENTER","WEAVER","GREENE","LAWRENCE","ELLIOTT","CHAVEZ","SIMS","AUSTIN","PETERS","KELLEY","FRANKLIN","LAWSON","FIELDS","GUTIERREZ","RYAN","SCHMIDT","CARR","VASQUEZ","CASTILLO","WHEELER","CHAPMAN","OLIVER","MONTGOMERY","RICHARDS","WILLIAMSON","JOHNSTON"],
  "city": ["New York","Los Angeles","Chicago","Houston","Philadelphia","Phoenix","San Antonio","San Diego","Dallas","San Jose","Austin","Jacksonville","San Francisco","Indianapolis","Columbus","Fort Worth","Charlotte","Detroit","El Paso","Seattle","Denver","Washington","Memphis","Boston","Nashville","Baltimore","Oklahoma City","Portland","Las Vegas","Louisville","Milwaukee","Albuquerque","Tucson","Fresno","Sacramento","Long Beach","Kansas City","Mesa","Atlanta","Virginia Beach","Omaha","Colorado Springs","Raleigh","Miami","Oakland","Minneapolis","Tulsa","Cleveland","Wichita","New Orleans","Arlington","Bakersfield","Tampa","Aurora","Honolulu","Anaheim","Santa Ana","Corpus Christi","Riverside","St. Louis","Lexington","Pittsburgh","Stockton","Anchorage","Cincinnati","Saint Paul","Greensboro","Toledo","Newark","Plano","Henderson","Lincoln","Orlando","Jersey City","Chula Vista","Buffalo","Fort Wayne","Chandler","St. Petersburg","Laredo","Durham","Irvine"],
  "bankRoutingNumber": ["001001001","028028028","348348348","487487487","785785785","658658658","896896896"],
  "bankAccountNumber": ["16001","02847","47963","30564","4851","658028","1876","8569","8748"],
  "accountType": ["Checking","Savings"],
  "purpose": ["Home Improvement","Personal","Electronics","Tour","Medical","Consumer Loan"],
  "monthlyIncome": ["5000","4000","8500","9500","15000","10000","6500","17500","12345","8000","9000","10000","11000","12000","13000","14000","15000","16000","17000","18000","19000","20000","4150","4750","5350","5950","6550","7150","7750","8350","8950"],
  "bankNames": ["ACME Bank", "Bank Of America","Chase","Union Bank","American Express","Bank Of California","Bank Of Brazil","Bradesco","HSBC","Ally","CitiBank","Wells Fargo"],
  "state": ["CA","TX","KS","LV"]
}

var LoanApi = function(configuration) {
  if (typeof configuration === 'undefined' || configuration == null)
    throw 'Parameter cannot be null: configuration';

  this.configuration = configuration;
  this.http = new Http(configuration.token);
};

LoanApi.prototype.get = function getLoanInfo(refNumber) {
  return this.http.get(this.configuration.services.loans, util.format('/%s/info', refNumber)).
    then(function(response) {
      return response.body;
    });
};

LoanApi.prototype.getCurrentDue = function getCurrentDue(refNumber, date) {
  return this.http.get(this.configuration.services.loans, util.format('/schedule/%s/current-due/%s', refNumber, date)).
    then(function(response) {
      return parseFloat(response.body);
    });
};
LoanApi.prototype.onboard = function onboard(refNumber, terms) {
  return this.http.post(this.configuration.services.loans, '/onboard', [{
      "referenceNumber": refNumber,
      "terms": {
          "loanAmount": terms.loanAmount,
          "applicationDate": terms.originationDate,
          "originationDate": terms.originationDate,
          "fundedDate": terms.originationDate,
          "term": terms.term,
          "rate": terms.rate,
          "rateType": "Percent",
          "fees": [
            {
                "code": "142",
                "type": "Fixed",
                "amount": 100
            }
        ]
      },
      "fundingSource": "Goldman Sachs",
      "purpose": random(dataset.purpose),
      "investor": {
        "id": "1001",
        "loanPurchaseDate": terms.originationDate
      },
      "scores": [
        {
            "name": "FICO",
            "initialScore": Math.round(Math.random() * 700) + 300,
            "initialDate": terms.originationDate,
            "updatedScore": Math.round(Math.random() * 700) + 300,
            "updatedDate": terms.originationDate
        }
    ],
      "grade": random(["A","B","C","D","E"]),
      "preCloseDti": Math.round(Math.random() * 9) + 1,
      "postCloseDti": Math.round(Math.random() * 9) + 1,
      "monthlyIncome": random(dataset.monthlyIncome),
      "homeOwnership": random(["Owner","Rental"]),
      "campaignCode": null,
      "paymentMethod": terms.paymentMethod || random(["ACH","Check","ACH","Check","ACH","Check","ACH","Check","ACH","Check","ACH","Check","ACH","Check"]),
      "bankInfo": {
        "routingNumber": random(dataset.bankRoutingNumber),
        "accountNumber": random(dataset.bankAccountNumber),
        "accountType": random(dataset.accountType),
        "bankName": random(dataset.bankNames)
      },
      "borrowers": [
        {
          "referenceNumber": "00001",
          "firstName": camelize(random(dataset.firstNames)),
          "middleInitial": random(["A","B","C","D","E"]),
          "lastName": camelize(random(dataset.lastNames)),
          "dateOfBirth": "1970-01-01",
          "ssn": "0000000",
          "email": "support@lendfoundry.com",
          "isPrimary": true,
          "addresses": [
            {
              "line1": "000 Main St",
              "line2": null,
              "city": random(dataset.city),
              "state": random(dataset.state),
              "zipCode": "00000",
              "isPrimary": true
            }
          ],
          "phones": [
            {
              "isPrimary": true,
              "number": "90909090",
              "type": "Home"
            }
          ]
        }
      ]
  }]);
};

function random(values) {
  var index = Math.round(Math.random() * (values.length - 1));
  return values[index];
}

function camelize(str) {
  return str.substr(0,1).toUpperCase() + str.substr(1).toLowerCase();
}

module.exports = LoanApi;
