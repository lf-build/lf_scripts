var Q = require('q');
var unirest = require('unirest');

function SecureHttp(token) {
  this.token = token;
}

SecureHttp.prototype.get = function get(service, path) {
  var deferred = Q.defer();
  var url = buildUrl(service, path);
  unirest.get(url)
    .header('Authorization', this.token)
    .header('Accept', 'application/json')
    .header('Content-Type', 'application/json')
    .send()
    .end(function (response) {
      if(response.status >= 200 && response.status < 300) {
        deferred.resolve(response);
      } else {
        console.log('ERROR: request failed: %s\n%s', url, response.raw_body || JSON.stringify(response, null, 2));
        deferred.reject(new Error(response.raw_body));
      }
    });
  return deferred.promise;
}

SecureHttp.prototype.post = function post(service, path, body) {
  var deferred = Q.defer();
  var url = buildUrl(service, path);
  unirest.post(url)
    .header('Authorization', this.token)
    .header('Accept', 'application/json')
    .header('Content-Type', 'application/json')
    .send(JSON.stringify(body))
    .end(function (response) {
      if(response.status >= 200 && response.status < 300) {
        deferred.resolve(response);
      } else {
        console.log('ERROR: request failed: %s\n%s', url, response.raw_body || JSON.stringify(response, null, 2));
        deferred.reject(new Error(response.raw_body));
      }
    });
  return deferred.promise;
};

function buildUrl(service, path) {
  if(typeof service === 'undefined' || service === null) throw "Argument for parameter 'service' is null or undefined";
  if(typeof service.host === 'undefined' || service.host === null) throw "Argument for parameter 'service' is null or undefined";
  var url = 'http://' + service.host;
  if (service.port) url += ':' + service.port;
  if (service.root) url += service.root;
  if (path) url += path;
  return url;
}

module.exports = SecureHttp;
