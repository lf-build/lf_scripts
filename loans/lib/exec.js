var Q = require('q');
var child_process = require('child_process');

module.exports = function exec(cmd, args, options, output) {
  var deferred = Q.defer();
  var spawn = child_process.spawn;
  var child = spawn(cmd, args, options || {});

  output = output || function(buffer) { console.log(buffer.toString()); };

  child.stdout.on('data', function (buffer) {
    output(buffer);
  });

  child.stderr.on('data', function (buffer) {
    console.log(arguments);
    console.log(buffer.toString());
  });

  child.on('close', function(code) {
    if (code === 0)
      deferred.resolve();
    else
      deferred.reject({exitCode: code});
  });

  return deferred.promise;
}
