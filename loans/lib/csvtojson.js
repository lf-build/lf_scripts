var fs = require('fs');
var newlineChars = {
  windows: '\r\n',
  unix: '\n'
};

function trim(s) {
  var r = s.trim();
  return r.length > 0 ? r : null;
}

function getNewLineChar(str) {
  if (str.indexOf(newlineChars.windows) > -1)
    return newlineChars.windows;
  else
    return newlineChars.unix;
}

module.exports = function csvToJson(file) {
  var csv = fs.readFileSync(file, 'utf8');
  var newline = getNewLineChar(csv);
  var lines = csv.split(newline);
  var header = lines[0].split(',').map(trim);
  var rows = [];

  for(var row = 1; row < lines.length; row++) {
    var obj = {};
    var line = lines[row];
    if(line && line.trim() !== '') {
      for(var column = 0; column < header.length; column++) {
        var values = line.split(',').map(trim);
        var name = header[column];
        obj[name] = values[column];
      }
      rows.push(obj);
    }
  }

  return rows;
}
