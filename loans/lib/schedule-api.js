var Q = require('q');
var moment = require('moment');
var fs = require('fs');
var os = require('os');
var util = require('util');
var newline = require('os').EOL;
var Http = require('./secure-http.js');
var exec = require('./exec.js');
var path = require('path');

var ScheduleApi = function ScheduleApi(configuration) {
  if (typeof configuration === 'undefined' || configuration == null)
    throw 'Parameter cannot be null: configuration';

  this.configuration = configuration;
  this.http = new Http(configuration.token);
}

ScheduleApi.prototype.getSchedule = function getSchedule(refNumber) {
  return this.http.get(this.configuration.services.loans, util.format('/%s/schedule', refNumber)).
    then(function(response) {
      var installments = response.body;
      return installments;
    });
};

ScheduleApi.prototype.getScheduleAsCsv = function getSchedule(refNumber) {
  return this.getSchedule(refNumber).
    then(function(installments) {
      return toCsv(installments, refNumber);
    });
};

ScheduleApi.prototype.exportScheduleAsCsv = function getSchedule(refNumber) {
  var configuration = this.configuration;
  return this.getScheduleAsCsv(refNumber).then(function(csv) {
    return saveCsvToFile(csv, refNumber, configuration);
  });
};

function formatDate(date) {
  return date ? moment(date.substr(0, 10)).format('YYYY-MM-DD') : '';
}

function toCsv(installments, refNumber) {
  var csv = util.format('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s' + newline,
    'AnniversaryDate',
    'DueDate',
    'PaymentDate',
    'PaymentId',
    'OpeningBalance',
    'PaymentAmount',
    'Interest',
    'Principal',
    'EndingBalance',
    'Status',
    'Type',
    'PaymentMethod'
  );

  installments.forEach(function(installment) {
    csv += util.format('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s' + newline,
      formatDate(installment.anniversaryDate),
      formatDate(installment.dueDate),
      formatDate(installment.paymentDate),
      installment.paymentId,
      installment.openingBalance,
      installment.paymentAmount,
      installment.interest,
      installment.principal,
      installment.endingBalance,
      installment.status,
      installment.type,
      installment.paymentMethod
    );
  });

  return csv;
}

function saveCsvToFile(csv, refNumber, configuration) {
  var dir = configuration.output || os.tmpdir();
  var filename = util.format("schedule-%s-%d.csv", refNumber, new Date().getTime());
  var filepath = path.join(dir, filename);
  var deferred = Q.defer();
  fs.writeFile(filepath, csv, function(err) {
      if(err) deferred.reject(err);
      deferred.resolve(filepath);
  });
  return deferred.promise;
}

module.exports = ScheduleApi;
