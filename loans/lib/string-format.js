function toCamelCase(s) {
	if (!s || s.trim() === '') return s;
	return s.substr(0, 1).toLowerCase() + s.substr(1);
}

function isDate(s) { return isString(s) && s.match(/^\d\d\d\d-\d\d-\d\d$/); }
function isNumber(s) { return isString(s) && s.match(/^-?[\d]+(\.[\d]+)?$/); }
function isString(s) { return typeof s === 'string'; }

module.exports = {
  toCamelCase: toCamelCase,
  isDate: isDate,
  isNumber: isNumber,
  isString: isString
};
