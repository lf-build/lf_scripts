var Q = require('q');
var util = require('util');
var Http = require('./secure-http.js');
var WebSocket = require('ws');

function isEmpty(v) {
  return typeof v === 'undefined' || v === null || v === '';
}

var EventHubApi = function EventHubApi(configuration) {
  if (isEmpty(configuration)) throw 'Parameter is null or empty: configuration';
  if (isEmpty(configuration.token)) throw 'Parameter is null or empty: configuration.token';
  if (isEmpty(configuration.services)) throw 'Parameter is null or empty: configuration.services';
  if (isEmpty(configuration.services.events)) throw 'Parameter is null or empty: configuration.services.events';

  this.configuration = configuration;
  this.http = new Http(configuration.token);
  this.handlers = {};
  this.ws = {};
}

EventHubApi.prototype.publish = function publish(name, data) {
  return this.http.post(this.configuration.services.events, '/' + name, data);
}

EventHubApi.prototype.handle = function handle(name, handler) {
  this.handlers[name] = handler;
}

EventHubApi.prototype.listen = function listen(eventName) {
  if(this.ws[eventName]) {
    console.log('warning: already listening for %s events', eventName);
    return;
  }

  var self = this;
  var deferred = Q.defer();

  var url = util.format('ws://%s:%s/%s', this.configuration.services.events.host, this.configuration.services.events.wsport, eventName);

  var ws = new WebSocket(url);

  ws.on('open', function() {
    deferred.resolve();
  });

  ws.on('message', function(data, flags) {
    var eventData = JSON.parse(data);
    var eventName = eventData.name;
    var handler = self.handlers[eventName];
    if (handler) {
      self.handlers[eventName] = null;
      handler(data, flags);
    }
  });

  this.ws[eventName] = ws;

  return deferred.promise;
}

EventHubApi.prototype.close = function close() {
  for(var key in this.ws) {
    var ws = this.ws[key];
    ws.close();
  }
};

module.exports = EventHubApi;
