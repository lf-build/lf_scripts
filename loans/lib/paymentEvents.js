var EventHubApi = require('./eventhub-api.js');

var PaymentEvents = function(configuration) {
  var eventhub = new EventHubApi(configuration);

  function paymentEvent(refNumber, paymentStatus, data) {
    return {
      "loanReferenceNumber":refNumber,
      "dueDate": data.dueDate,
      "amount": data.amount,
      "paymentId": data.paymentId,
      "type": data.type,
      "status": paymentStatus,
      "attempts": 1,
      "code": "124",
      "method": data.method || "ACH"
    };
  }

  function sendPaymentEvent(eventName, eventData) {
    console.log(eventData.dueDate, eventData.paymentId, eventData.type, eventData.amount, eventData.status);
    return eventhub.publish(eventName, eventData);
  };

  this.add = function(refNumber, data) { return sendPaymentEvent('PaymentAdded', paymentEvent(refNumber, 'Pending', data)); };
  this.receive = function(refNumber, data) { return sendPaymentEvent('PaymentReceived', paymentEvent(refNumber, 'Received', data)); };
  this.fail = function(refNumber, data) { return sendPaymentEvent('PaymentFailed', paymentEvent(refNumber, 'Failed', data)); };
};

module.exports = PaymentEvents;
