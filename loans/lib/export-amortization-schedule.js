var moment = require('moment');
var fs = require('fs');
var util = require('util');
var newline = require('os').EOL;
var services = require('../../cfg/services.js');
var post = require('../../lib/http-post.js');
var exec = require('../../lib/exec.js');
var path = require('path');

function exportSchedule(terms) {
  var path = util.format('/payment/schedule/%s', moment(terms.noteDate).format("YYYY-MM-DD"));
  post(services.amortization, path, terms).then(function(response) {
    try {
      var installments = response.body.payments;
      toCsv(installments);
    } catch (e) {
      console.log('error:',e);
    }
  });
}

function formatDate(date) {
  return date ? moment(date.substr(0, 10)).format('YYYY-MM-DD') : '';
}

function toCsv(installments) {
  var csv = util.format('%s,%s,%s,%s,%s,%s,%s' + newline,
    'Date',
    'AnniversaryDate',
    'OpeningBalance',
    'PaymentAmount',
    'PrincipalAmount',
    'InterestAmount',
    'EndingBalance'
  );

  installments.forEach(function(installment) {
    csv += util.format('%s,%s,%s,%s,%s,%s,%s' + newline,
      formatDate(installment.date),
      formatDate(installment.anniversaryDate),
      installment.openingBalance,
      installment.paymentAmount,
      installment.principalAmount,
      installment.interestAmount,
      installment.endingBalance
    );
  });

  var fileName = util.format(path.join("output", "amortization-schedule-%s.csv"), new Date().getTime());

  fs.writeFile(fileName, csv, function(err) {
      if(err) return console.log(err);
      console.log("File saved: "+fileName);
      exec('C:\\Program Files\\Microsoft Office 15\\root\\office15\\EXCEL.EXE', [path.resolve(fileName)]);
  });
}

module.exports = exportSchedule;
