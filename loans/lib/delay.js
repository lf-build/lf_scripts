var Q = require('q');

module.exports = {
  promise: function(ms) {
    return function() {
      var deferred = Q.defer();
      setTimeout(deferred.resolve, ms);
      return deferred.promise;
    };
  }
};
