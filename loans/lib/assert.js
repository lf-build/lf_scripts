var abort = require('./abort.js');
var util = require('util');

module.exports = {
  equal: function(expected, actual, msg) {
    if (expected !== actual)
      abort(util.format('%s expected=%s  actual=%s', 'ASSERTION FAILED: ' + (msg ? msg + ': ' : ''), expected, actual));
  }
};