var Http = require('./secure-http.js');
var util = require('util');

var ConfigurationApi = function(configuration) {
  if (typeof configuration === 'undefined' || configuration == null)
    throw 'Parameter cannot be null: configuration';

  this.configuration = configuration;
  this.http = new Http(configuration.token);
};

ConfigurationApi.prototype.set = function getSchedule(key, value) {
  return this.http.post(this.configuration.services.configuration, util.format('/%s', key), value).
    then(function(response) {
      return response.body;
    });
};

module.exports = ConfigurationApi;
