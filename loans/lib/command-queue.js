var Q = require('q');
var util = require('util');
var abort = require('./abort.js');

function CommandQueue () {
  this.queue = [];
  return this;
};

CommandQueue.prototype.add = function run(command) {
  this.queue.push(command);
  return this;
}

CommandQueue.prototype.run = function run(options) {
  var interval = options && options.interval ? parseInt(options.interval) : 0;
  var queue = this.queue;
  var index = 0;
  function runNextCommand() {
    var command = queue[index++];
    if (command) {
      var promise;
      try {
        promise = command();
      }
      catch(e) {
        console.log('error executing command #%d:', index, e);
      }
      if (promise) {
        if(interval > 0) {
          promise.then(delay(interval).then(runNextCommand).catch(onError));
          promise.catch(onError);
        }
        else {
          promise.then(runNextCommand);
          promise.catch(onError);
        }
      }
    }
  };
  function delay(interval) {
    var deferred = Q.defer();
    setTimeout(deferred.resolve, interval);
    return deferred.promise;
  }
  function onError() {
    abort(util.format('command #%d failed: ', index, JSON.stringify(arguments)));
  }
  runNextCommand();
}

module.exports = CommandQueue;
