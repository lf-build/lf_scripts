var fs = require('fs');
var path = require('path');
var LoanApi = require('./lib/loan-api.js');
var ScheduleApi = require('./lib/schedule-api.js');
var CommandQueue = require('./lib/command-queue.js');
var Excel = require('./lib/excel.js');
var abort = require('./lib/abort.js');
var PaymentEvents = require('./lib/paymentEvents.js');
var readConfig = require('./lib/read-config.js');

var args = process.argv.slice(2);
var configName = args[0];
var specName = args[1];

if (!configName || !specName)
  abort('Usage run-payments <config> <spec>');

start();

function getUserHome() {
  return process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'];
}

function start() {
  var spec = parseSpec(specName);
  var config = readConfig(configName);
  var loan = new LoanApi(config);
  var schedule = new ScheduleApi(config);
  var events = new PaymentEvents(config);
  var refNumber = getRefNumber(spec);
  var queue = new CommandQueue();

  queue.add(function() {
    return loan.onboard(refNumber, spec.terms);
  });

  if (spec.payments) {
    spec.payments.forEach(function(payment) {
      queue.add(function() {
        var executePayment = events[payment.operation];
        if (!executePayment)
          abort('Invalid payment operation: ' + payment.operation);
        return executePayment(refNumber, payment.data);
      });
    });
  }

  queue.add(function() {
    return schedule.exportScheduleAsCsv(refNumber).
      then(function(csvFile) {
        console.log('Schedule saved to %s', csvFile);
        return Excel.open(csvFile);
      });
  });

  queue.run({ interval: 3000 });
}

function parseSpec(name) {
  var homeDir = path.join(getUserHome(), '.lf');
  var specsDir = path.join(homeDir, 'specs');
  var file = path.join(specsDir, name + '.json');
  var json = JSON.parse(fs.readFileSync(file, 'utf8'));
  if (!json) abort('failed to parse spec file: ' + file);
  if (!json.referenceNumber) abort('spec error: referenceNumber not defined');
  if (!json.terms) abort('spec error: terms not defined');
  return json;
}

function getRefNumber(spec) {
  var timestamp = new Date().getTime().toString();
  return spec.referenceNumber.toString() + '-' + timestamp.substr(timestamp.length - 4);
}
