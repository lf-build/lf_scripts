var exportSchedule = require('./lib/export-amortization-schedule.js');

var args = process.argv.slice(2);

exportSchedule({
  noteDate: args[0],
  "Amount": 17500,
  "Rate": 12.84,
  "Term": 60,
  "Fees": [
      {"Type": "Fixed", "Amount": 875}
  ]
});
