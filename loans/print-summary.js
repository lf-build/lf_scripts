var LoanApi = require('./lib/loan-api.js');
var ScheduleApi = require('./lib/schedule-api.js');
var readConfig = require('./lib/read-config.js');
var Excel = require('./lib/excel.js');
var CommandQueue = require('./lib/command-queue.js');
var moment = require('moment-timezone');
var args = process.argv.slice(2);

if (args.length < 2) {
  console.log('usage: print-summary <config> <refnumber1> [refnumber2] [refnumber3]...')
  process.exit(1);
}

var configName = args[0];
var refNumbers = args.slice(1);
var config = readConfig(configName);
var loanApi = new LoanApi(config);
var scheduleApi = new ScheduleApi(config);
var queue = new CommandQueue();

refNumbers.forEach(function(refNumber) {
  queue.add(function() {
    return loanApi.get(refNumber).then(function(loan) {
      console.log('loan                  : %s %s $%s x%s %s%%',
        refNumber,
        moment.tz(loan.terms.originationDate.substr(0, 10), config.timezone).format('YYYY-MM-DD'),
        parseFloat(loan.terms.loanAmount),
        parseInt(loan.terms.term),
        parseFloat(loan.terms.rate)
      );
      console.log('currentDue            : %s', loan.currentDue);
      console.log('daysPastDue           : %s', loan.daysPastDue);
      console.log('remainingBalance      : %s', loan.remainingBalance);
      console.log('nextDueDate           : %s', loan.nextDueDate);
      console.log('nextInstallmentAmount : %s', loan.nextInstallmentAmount);
      console.log('-----');
    });
  });
});

queue.run({interval: 250});
