var abort = require('./lib/abort.js');
var delay = require('./lib/delay.js');
var path = require('path');
var readConfig = require('./lib/read-config.js');
var loadJson = require('./lib/load-json.js');
var CommandQueue = require('./lib/command-queue.js');
var ConfigurationApi = require('./lib/configuration-api.js');
var EventHubApi = require('./lib/eventhub-api.js');
var LoanApi = require('./lib/loan-api.js');
var Q = require('q');
var ScheduleApi = require('./lib/schedule-api.js');

function usage() {
  abort('Usage run-payments <config> <spec1> [spec2] [spec3]...');
}

var paths = {
  specs: path.join('tests', 'specs')
};

var commands = {
  'verify-schedule'    : require('./tests/commands/verify-schedule.js'),
  'verify-current-due' : require('./tests/commands/verify-current-due.js'),
  'payment-added'      : require('./tests/commands/payment-events.js').added,
  'payment-received'   : require('./tests/commands/payment-events.js').received,
  'payment-failed'     : require('./tests/commands/payment-events.js').failed
};

function main() {
  var args = process.argv.slice(2);
  var config = args[0];
  var specs = args.slice(1);

  if (!config || specs.length === 0)
    usage();

  start(specs, readConfig(config));
}

function start(specs, config) {
  var api = {
    loan: new LoanApi(config),
    schedule: new ScheduleApi(config),
    eventhub: new EventHubApi(config),
    configuration: new ConfigurationApi(config)
  };

  api.eventhub.listen('LoanUpdated').then(function() {
    var queue = new CommandQueue();

    var enqueueCommands = function(specName) {
      var spec = parseSpec(specName);
      var refNumber = getRefNumber(specName);

      queue.add(function() {
        console.log('--- %s %s', specName, '-----------------------------------------------------'.substr(specName.length));
        return Q();
      });

      if (spec.calendar) {
        queue.add(function() {
          console.log('configure calendar');
          return api.configuration.set('calendar/dates', spec.calendar).then(delay.promise(10000));
        });
      }

      queue.add(function() {
        console.log('onboard loan %s', refNumber);
        return api.loan.onboard(refNumber, spec.terms);
      });

      if (spec.commands) {
        spec.commands.forEach(function(data) {
          queue.add(getCommand(data, {
            commandName: data.name,
            refNumber: refNumber,
            config: config,
            api: api
          }));
        });
      }
    };

    specs.forEach(enqueueCommands);

    queue.add(function() {
      api.eventhub.close();
    });

    queue.run();
  });
}

function getCommand(data, context) {
  var name = data.name;

  if(!name || name.trim().length == 0)
    abort('command name not specified: %s', JSON.stringify(data));

  var build = commands[name];

  if(!build)
    abort('no command found with name "'+name+'"');

  return build(data, context);
}

function parseSpec(name) {
  var file = path.join(paths.specs, name + '.json');
  var json = loadJson(file);
  if (!json) abort('failed to parse spec file: ' + file);
  if (!json.terms) abort('spec error: terms not defined');
  return json;
}

function getRefNumber(specName) {
  var timestamp = new Date().getTime().toString();
  return specName + '-' + timestamp.substr(timestamp.length - 4);
}

main();
