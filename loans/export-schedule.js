var LoanApi = require('./lib/loan-api.js');
var ScheduleApi = require('./lib/schedule-api.js');
var readConfig = require('./lib/read-config.js');
var Excel = require('./lib/excel.js');
var CommandQueue = require('./lib/command-queue.js');
var moment = require('moment-timezone');
var args = process.argv.slice(2);

if (args.length < 2) {
  console.log('usage: export-schedule <config> <refnumber1> [refnumber2] [refnumber3]...')
  process.exit(1);
}

var configName = args[0];
var refNumbers = args.slice(1);
var config = readConfig(configName);
var loanApi = new LoanApi(config);
var scheduleApi = new ScheduleApi(config);
var queue = new CommandQueue();

refNumbers.forEach(function(refNumber) {
  queue.add(function() {
    return loanApi.get(refNumber).then(function(loan) {
      console.log('Origination    : %s', moment.tz(loan.terms.originationDate.substr(0, 10), config.timezone).format('YYYY-MM-DD'));
      console.log('Amount         : %s', parseFloat(loan.terms.loanAmount));
      console.log('Term           : %s', parseInt(loan.terms.term));
      console.log('Rate           : %s', parseFloat(loan.terms.rate)/100.0);
      console.log('Monthly        : %s', parseFloat(loan.terms.rate)/100.0/12.0);
      console.log('Daily          : %s', parseFloat(loan.terms.rate)/100.0/360.0);
      console.log('Payment method : %s', loan.paymentMethod);
      console.log('Status         : %s %s', loan.status.code, loan.status.description);
      return scheduleApi.exportScheduleAsCsv(refNumber).
        then(function(csvFile) {
          console.log('Schedule saved to %s', csvFile);
          return Excel.open(csvFile);
        });
    });
  });
});

queue.run({interval: 1000});
