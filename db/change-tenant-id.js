var util = require('util');
var CommandQueue = require('./lib/command-queue.js');
var exec = require('./lib/exec.js');
var args = process.argv.slice(2);

if (args.length !== 3) {
   console.log('Usage: change-tenant-id <host> <fromTenantId> <toTenantId>');
   process.exit(1);
}

var collections = {
  "accounting"        : ["journal"],
  "ach"               : ["ach_file", "ach_instructions", "counter", "file", "instructions"],
  "activity-log"      : ["activitylog"],
  "event-store"       : ["events", "event-store"],
  "identity"          : ["sessions", "users"],
  "loans-backup"      : ["loan-backup"],
  "loan-tape"         : ["counters", "daily-investor-feed", "daily-transaction-tape", "monthly-loan-tape"],
  "loan-transaction"  : ["transactions"],
  "loans"             : ["borrowers", "fees", "loan-filters", "loans", "payments", "schedule"],
  "loans-filters"     : ["loan-filters"],
  "notifications"     : ["templates"],
  "status-management" : ["status-management"]
};

start();

function start() {
  var host = args[0];
  var fromTenantId = args[1];
  var toTenantId = args[2];
  var commandQueue = new CommandQueue();
  for (var db in collections) {
    collections[db].forEach(function(collection){
      commandQueue.add(changeTenantId(host, db, collection, fromTenantId, toTenantId));
    });
  }
  commandQueue.add(replaceTenantEntry(host, fromTenantId, toTenantId));
  commandQueue.run();
}

function changeTenantId(host, db, collection, fromTenantId, toTenantId) {
  return function() {
    console.log('changing tenant id "%s" to "%s" in %s/%s', fromTenantId, toTenantId, db, collection);
    return exec('mongo', [
      host+'/'+db,
      '--eval',
      util.format('db.getCollection("%s").update({TenantId:"%s"}, { $set: {TenantId:"%s"} }, {multi:true});', collection, fromTenantId, toTenantId)
    ]);
  };
}

function replaceTenantEntry(host, oldTenantId, newTenantId) {
  return function() {
    var db = 'tenant';
    var collection = 'Tenant';
    var insertion = util.format('db.getCollection("%s").insert({"_id":"%s","Name":"%s","Email":"%s@dev.lendfoundry.com","Phone":"000 000 0000","Website":"http://lendfoundry.com","IsActive":true});', collection, newTenantId, newTenantId, newTenantId);
    var removal = util.format('db.getCollection("%s").remove({_id:"%s"});', collection, oldTenantId);

    console.log('replacing tenant "%s" with "%s"', oldTenantId, newTenantId);

    return exec('mongo', [host+'/'+db, '--eval', removal]).then(function() {
      return exec('mongo', [host+'/'+db, '--eval', insertion]);
    });
  };
}
