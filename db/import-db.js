var path = require('path');
var ls = require('./lib/ls.js');
var CommandQueue = require('./lib/command-queue.js');
var exec = require('./lib/exec.js');
var abort = require('./lib/abort.js');
var args = process.argv.slice(2);

if (args.length !== 2)
   abort('Usage: import-db <dest_host> <source_dir>');

start();

function start() {
  var host = args[0];
  var dir = args[1];
  var commandQueue = new CommandQueue();
  ls.dirs(dir).forEach(function(subdir) {
    var db = path.basename(subdir);
    commandQueue.add(importCommand(subdir, db, host));
  });
  commandQueue.run();
}

function importCommand(dir, db, host) {
  return function() {
    console.log('importing %s into %s', dir, db);
    return exec('mongorestore', [
      '--host='+host,
      '--drop',
      '--gzip',
      dir
    ]);
  };
}
