var args = process.argv.slice(2);
var abort = require('./lib/abort.js');

if (args.length < 1 || args.length > 2)
   abort('Usage: dump-db <config> [output_dir]');

var fs = require('fs');
var path = require('path');
var CommandQueue = require('./lib/command-queue.js');
var readConfig = require('./lib/read-config.js');
var exec = require('./lib/exec.js');

function run() {
  var configName = args[0];
  var config = readConfig(configName).db;
  var dbs = Object.keys(config.collections);
  var commandQueue = new CommandQueue();
  var root = args[1] || path.join('output', 'export_' + configName + '_' + new Date().getTime());

  console.log('output dir:', root);
  console.log('dbs:', dbs.join(', '));

  dbs.forEach(function(db) {
    commandQueue.add(exportCommand(db, config, root));
  });

  commandQueue.run();
}

function parseConfig(file) {
  var config = JSON.parse(fs.readFileSync(file, 'utf8'));
  if (!config) abort('failed to parse config file: '+file);
  if (!config.host) abort('config error: host not defined');
  if (!config.collections) abort('config error: collections not defined');
  if ((typeof config.collections) !== 'object') abort('config error: collections should be an array');
  return config;
}

function exportCommand(db, config, root) {
  return function() {
    console.log('exporting db "%s"', db);
    var options = [
      '--host='+config.host,
      '--db='+db,
      '--out='+root+'/'+db,
      '--gzip'
    ];
    if (config.auth && config.auth.enabled) {
      options.push('--username='+config.auth.username);
      options.push('--password='+config.auth.password);
      options.push('--authenticationDatabase=admin');
    }
    return exec('mongodump', options).then(() => console.log('finished:', root));
  };
}

run();
