var path = require('path');
var ls = require('./lib/ls.js');
var CommandQueue = require('./lib/command-queue.js');
var exec = require('./lib/exec.js');
var  abort = require('./lib/abort.js');
var args = process.argv.slice(2);

if (args.length !== 1) {
   console.log('Usage: obfuscate <host>');
   process.exit(1);
}

start();

function start() {
  var host = args[0];
  var commandQueue = new CommandQueue();
  ls.files('./pii-obfuscation').forEach(function(scriptFile) {
    var db = path.basename(scriptFile, '.js');
    commandQueue.add(obfuscationCommand(host, db, scriptFile));
  });
  commandQueue.run();
}

function obfuscationCommand(host, db, scriptFile) {
  return function() {
    console.log('obfuscating piis in %s', db);
    return exec('mongo', [
      host+'/'+db,
      scriptFile
    ]);
  };
}
