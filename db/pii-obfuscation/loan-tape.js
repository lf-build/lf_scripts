obfuscate();

function obfuscate() {
  db.getCollection('loan-tapes').find({}).forEach(function(tape){
    tape.BorrowerFirstName = randomText(10);
    tape.BorrowerLastName = randomText(10);
    tape.State = randomText(2);
    tape.ZipCode = randomNumber(5);
    db.getCollection('loan-tapes').update({_id: tape._id}, tape);
  });
}

function randomNumber(length) { return randomString(length, "0123456789"); }
function randomText(length) { return randomString(length, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"); }
function randomString(length, chars) {
    var text = "";
    for(var i = 0; i < length; i++)
        text += chars.charAt(Math.floor(Math.random() * chars.length));
    return text;
}
