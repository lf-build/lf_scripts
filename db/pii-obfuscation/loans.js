obfuscate();

function obfuscate() {
  var customerNames = {};

  db.borrowers.find({}).forEach(function(borrower) {
    borrower.Email = randomText(10) + "@dev.lendfoundry.com";
    borrower.FirstName = randomText(10);
    borrower.LastName = randomText(10);
    borrower.MiddleInitial = randomText(1);
    borrower.SSN = randomNumber(9);
    borrower.Phones.forEach(function(phone){
      phone.Number = randomNumber(10);
    });
    borrower.Addresses.forEach(function(address) {
      address.Line1 = randomText(10);
      address.Line2 = randomText(10);
      address.City = randomText(10);
      address.State = randomText(2);
      address.ZipCode = randomNumber(5);
    });
    db.borrowers.update({_id: borrower._id}, borrower);
    customerNames[borrower.LoanReferenceNumber.toString()] = borrower.FirstName + ' ' + borrower.LastName;
  });

  db.loans.find({}).forEach(function(loan) {
    if (loan.BankInfo) {
      loan.BankInfo.AccountNumber = randomNumber(10);
    }
    if (loan.BankAccounts) {
      loan.BankAccounts.forEach(function(account) {
        account.RoutingNumber = randomNumber(10);
        account.AccountNumber = randomNumber(10);
        account.BankName = 'ACME';
      });
    }
    loan.MonthlyIncome = randomNumber(5);
    db.loans.update({_id: loan._id}, loan);
  });

  db.getCollection('loan-filters').find({}).forEach(function(filter) {
    filter.CustomerName = customerNames[filter.LoanReferenceNumber.toString()];
    db.getCollection('loan-filters').update({_id: filter._id}, filter);
  });
}

function randomNumber(length) { return randomString(length, "0123456789"); }
function randomText(length) { return randomString(length, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"); }
function randomString(length, chars) {
    var text = "";
    for(var i = 0; i < length; i++)
        text += chars.charAt(Math.floor(Math.random() * chars.length));
    return text;
}
