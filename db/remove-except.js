var util = require('util');
var CommandQueue = require('../../lib/command-queue.js');
var exec = require('../../lib/exec.js');
var args = process.argv.slice(2);

if (args.length !== 2) {
   console.log('Usage: remove-except <tenantId> <host>');
   process.exit(1);
}

var collections = {
  "accounting"        : ["journal"],
  "activity-log"      : ["activitylog"],
  "event-store"       : ["events", "event-store"],
  "identity"          : ["sessions", "users"],
  "loan-tape"         : ["loan-tapes"],
  "loan-transaction"  : ["transactions"],
  "loans"             : ["borrowers", "fees", "loan-filters", "loans", "payments", "schedule"],
  "notifications"     : ["templates"],
  "tenant"            : ["Tenant"]
};

start();

function start() {
  var tenantId = args[0];
  var host = args[1];
  var commandQueue = new CommandQueue();
  for (var db in collections) {
    collections[db].forEach(function(collection){
      commandQueue.add(command(host, db, collection, tenantId));
    });
  }
  commandQueue.run();
}

function command(host, db, collection, tenantId) {
  return function() {
    console.log('remove everything from all tenants except "%s" in %s/%s', tenantId, db, collection);
    return exec('mongo', [
      host+'/'+db,
      '--eval',
      util.format('db.getCollection("%s").remove({TenantId: {$ne: "%s"}});', collection, tenantId)
    ]);
  };
}
