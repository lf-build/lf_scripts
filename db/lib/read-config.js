var fs = require('fs');
var path = require('path');
var loadJson = require('./load-json.js');

function getUserHome() {
  return process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'];
}

function fileExists(filePath) {
  try { return fs.statSync(filePath).isFile(); }
  catch (err) { return false; }
}

module.exports = function(configName) {
  var configDir = path.join(getUserHome(), '.lf');
  var configFile = path.join(configDir, configName + '.json');
  if (!fileExists(configFile)) {
    console.log('config file not found at %s', configFile);
    process.exit(1);
  }
  return loadJson(configFile);
}
