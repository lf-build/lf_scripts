var Q = require('q');
var child_process = require('child_process');

module.exports = function exec(cmd, args) {
  var deferred = Q.defer();
  var spawn = child_process.spawn;
  var child = spawn(cmd, args);

  child.stdout.on('data', function (buffer) {
    console.log(buffer.toString());
  });

  child.stderr.on('data', function (buffer) {
    console.log(arguments);
    console.log(buffer.toString());
  });

  child.on('close', function(code) {
    if (code === 0)
      deferred.resolve();
    else
      deferred.reject({exitCode: code});
  });

  return deferred.promise;
}
