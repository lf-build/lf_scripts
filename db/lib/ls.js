var fs = require('fs');
var path = require('path');

function ls(dir, predicate) {
  var entries = fs.readdirSync(dir);
  var files = [];
  entries.forEach(function(entry){
    var stat = fs.statSync(path.join(dir, entry));
    if (predicate(stat))
      files.push(path.join(dir, entry));
  });
  return files;
}

function isFile(stat) { return stat.isFile(); }
function isDirectory(stat) { return stat.isDirectory(); }

module.exports = {
  ls: ls,
  files: function(dir) { return ls(dir, isFile); },
  dirs: function(dir) { return ls(dir, isDirectory); }
}
