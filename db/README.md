# Getting Started

First make sure you have Nodejs and Mongodb installed and included in your
system path.

Then `cd` into this directory and run:

```
npm install
```

# Usage

## Export Databases

```
node export-db.js <config_file> [output_dir]
```

Configuration files are available in the `cfg` folder. If you omit the
`output_dir` parameter, a random directory will be created.

## Import Databases

```
node import-db.js <dest_mongodb_host> <db_dump_dir>
```

## Change The Tenant Id

```
node change-tenant-id.js <mongodb_host> <src_tenant_id> <dst_tenant_id>
```

## Obfuscate PII

```
node obfuscate-pii.js <mongodb_host>
```
