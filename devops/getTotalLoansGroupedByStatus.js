var tenantId = "rockloans"
var loansGroupedByStatusCode = db.loans.aggregate(
   [ {$match:{ "TenantId": tenantId }},
     { $group : { _id : "$Status.Code", loanReferenceNumber: { $push: "$ReferenceNumber" } } }
   ]
).result;

//printjson(loanAggregate)
var totalLoanCount = 0;
for(var i=0;i<loansGroupedByStatusCode.length;i++){
    print("Status Code:"+loansGroupedByStatusCode[i]._id + " Loans Count :"+ loansGroupedByStatusCode[i].loanReferenceNumber.length)
    // Used for monitoring loan status post daily onboarding
    if(loansGroupedByStatusCode[i]._id==100.02)
        print(loansGroupedByStatusCode[i].loanReferenceNumber)
    totalLoanCount = totalLoanCount + loansGroupedByStatusCode[i].loanReferenceNumber.length;      
}

print("Total Loans for Tenant:"+tenantId + " is: "+ totalLoanCount)