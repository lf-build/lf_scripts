var date = "2016-05-11"; 
var tenantId = "rockloans"
var paymentReceivedEventText=  'via ACH received as';
var eventName = "PaymentReceived"
var totalPaymentsToBeReconciled = 0;
var totalPaymentsPendingToBeReconciled = 0;
var totalPaymentsReconciled = 0;
var pendingLoansToBeReconciled = new Array();
var payments = new Array();
var paymentId = new Array();
var statusForTheDay =new Array();

function getTransactionsForPaymentIds(tenant,date, paymentIDs)
{
    var transactions = db.getSiblingDB('loan-transaction').getCollection('transactions').aggregate([
    {$match :{"TenantId" : tenant,"Date.DateTime":new ISODate(date + "T04:00:00.000Z"), "Code":"124", "PaymentId" : {$in : paymentIDs }}}
    ]).result;
    return transactions.length;
}

function getActivityLogForGivenCondition(date,eventName,searchText)
{
	var activityLogEntry = db.getSiblingDB('activity-log').getCollection('activitylog').aggregate([
	{
		$match :{"EventDate.Day":date+'-d', "EventName":eventName, 
		"Description" :{$regex: searchText} }
	}]).result;
	
	return activityLogEntry
}


var paymentsToReconcile = db.getCollection('schedule').aggregate([
{
	$unwind: "$Installments" 
},
{
	$match:{ "TenantId": tenantId, "Installments.PaymentMethod":1,
	"Installments.DueDate.DateTime" : new ISODate(date + "T04:00:00.000Z") } //use T05:00:00.000Z if DST
}, 
{
	$project:{ LoanReferenceNumber : 1,"Installments.PaymentMethod":1 ,"Installments.PaymentId":1,"Installments.DueDate.DateTime": 1, "Installments.PaymentAmount": 1, "Installments.Type": 1, "Installments.Status": 1}
}]).result;



for(var i=0;i<paymentsToReconcile.length;i++) {    
    paymentId.push(paymentsToReconcile[i].Installments.PaymentId)
    if(paymentsToReconcile[i].Installments.Status == 0)
    {
         totalPaymentsPendingToBeReconciled = totalPaymentsPendingToBeReconciled + 1;
         pendingLoansToBeReconciled.push({LoanReferenceNumber :paymentsToReconcile[i].LoanReferenceNumber })
         
    }else{
         totalPaymentsReconciled = totalPaymentsReconciled + 1;                  
     }
        
     // some of the fields commented below are used for manually Queuing the ACH in case the task fails. Refer the Payment service payload for clear understanding
		payments.push({
        LoanReferenceNumber : paymentsToReconcile[i].LoanReferenceNumber,
        DueDate : date,
        PaymentMethod: paymentsToReconcile[i].Installments.PaymentMethod,
        Amount : paymentsToReconcile[i].Installments.PaymentAmount,
        PaymentId : paymentsToReconcile[i].Installments.PaymentId,        
        Type : paymentsToReconcile[i].Installments.Type ,
        //Status: "Received",
        Status : paymentsToReconcile[i].Installments.Status == 0 ? "Scheduled" : "Completed",
        //Attempts: 1,
        //Code: "124",
        //Method: "ACH"
    });
}

reconciledTransactions = getTransactionsForPaymentIds(tenantId,date,paymentId);
activityLogData= getActivityLogForGivenCondition(date,eventName,paymentReceivedEventText)
statusForTheDay.push ({ Summary : {
    PaymentReconcileDate : date,
    TotalpaymentsToReconcile : paymentsToReconcile.length,    
    TotalPaymentsPendingToBeReconciled : totalPaymentsPendingToBeReconciled,    
    TotalPaymentsReconciled: totalPaymentsReconciled,
    TotalActivityLogEntries : activityLogData.length,
    TotalTransactionCreatedForPayments : reconciledTransactions    
    }});
    
statusForTheDay.push ({LoansPendingToBeReconciled : pendingLoansToBeReconciled});    
// Final Statistics
printjson(statusForTheDay)
printjson(payments);
