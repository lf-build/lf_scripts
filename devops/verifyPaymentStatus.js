var date = "2016-05-13"; 
var executionDate = "2016-05-12"
var tenantId = "rockloans"
var paymentAddedEventText=  'via ACH added as Scheduled for ' + date;

var activityLogEntry = db.getSiblingDB('activity-log').getCollection('activitylog').aggregate([{$match :{"EventDate.Day":executionDate+'-d', "EventName":'PaymentAdded', "Description" :{$regex: paymentAddedEventText} }}]).result;
var instructionsStatus = db.getSiblingDB('ach').getCollection('instructions').aggregate([{$match :{"ExecutionDate.DateTime":new ISODate(executionDate+ "T04:00:00.000Z")}}]).result;
var instructionsToBeQueued = db.getCollection('schedule').aggregate([
{ $unwind: "$Installments" },
{$match:{ "TenantId": tenantId, "Installments.PaymentMethod":1,"Installments.DueDate.DateTime" : new ISODate(date + "T04:00:00.000Z") }}, //use T05:00:00.000Z if DST
{$project:{ LoanReferenceNumber : 1,"Installments.PaymentMethod":1 ,"Installments.PaymentId":1,"Installments.DueDate.DateTime": 1, "Installments.PaymentAmount": 1, "Installments.Type": 1, "Installments.Status": 1}},
]).result;

var totalDebitAmount = 0;
var totalACHInstructions = 0;
var totalInstructionsPendingToBeQueued = 0;
var totalInstructionsQueued = 0;
var pendingLoansToBeQueued = new Array();
var payments = new Array();
var statusForTheDay =new Array();

for(var i=0;i<instructionsToBeQueued.length;i++) {
    if(instructionsToBeQueued[i].Installments.PaymentMethod==1)
    {
        totalDebitAmount = totalDebitAmount + instructionsToBeQueued[i].Installments.PaymentAmount;
        totalACHInstructions = totalACHInstructions + 1;       
     } 
     
     if(instructionsToBeQueued[i].Installments.PaymentId == null)
     {
         totalInstructionsPendingToBeQueued = totalInstructionsPendingToBeQueued + 1;
         pendingLoansToBeQueued.push({LoanReferenceNumber :instructionsToBeQueued[i].LoanReferenceNumber })
         
     } else
     {
         totalInstructionsQueued = totalInstructionsQueued + 1;         
         
     }
     
    
     // some of the fields commented below are used for manually Queuing the ACH in case the task fails. Refer the Payment service payload for clear understanding
    payments.push({
        LoanReferenceNumber : instructionsToBeQueued[i].LoanReferenceNumber,
        DueDate : date,
        PaymentMethod: instructionsToBeQueued[i].Installments.PaymentMethod,
        Amount : instructionsToBeQueued[i].Installments.PaymentAmount,
        PaymentId : instructionsToBeQueued[i].Installments.PaymentId,        
        Type : instructionsToBeQueued[i].Installments.Type ,
        //Status: "Received",
        Status : instructionsToBeQueued[i].Installments.Status == 0 ? "Scheduled" : "Completed",
        //Attempts: 1,
        //Code: "124",
        //Method: "ACH"
    });
}


statusForTheDay.push ({ Summary : {
    ACHInstructionDate : date,
    TotalInstructionsToBeQueued : totalACHInstructions,
    TotalDebitAmount : totalDebitAmount,
    TotalInstructionsPendingToBeQueued : totalInstructionsPendingToBeQueued,    
    TotalInstructionsQueued : totalInstructionsQueued,
    TotalActivityLogEntries : activityLogEntry.length,
    TotalEntriesInACH : instructionsStatus.length
    }});
    
statusForTheDay.push ({LoansPendingToBeQueued : pendingLoansToBeQueued});    
printjson(statusForTheDay)
printjson(payments);