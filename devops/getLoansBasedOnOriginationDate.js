var tenantId = "rockloans"
var loansGroupedByStatusCode = db.loans.aggregate(
   [ {$match:{ "TenantId": tenantId }},
     { $group : { _id : "$Terms.OriginationDate.DateTime", loanReferenceNumber: { $push: "$ReferenceNumber" } } }
   ]
).result;

//printjson(loanAggregate)
var totalLoanCount = 0;
for(var i=0;i<loansGroupedByStatusCode.length;i++){
   print(loansGroupedByStatusCode[i]._id + " - "+ loansGroupedByStatusCode[i].loanReferenceNumber)
    totalLoanCount = totalLoanCount + loansGroupedByStatusCode[i].loanReferenceNumber.length;      
}
printjson(loansGroupedByStatusCode)

//print("Total Loans for Tenant:"+tenantId + " is: "+ totalLoanCount)