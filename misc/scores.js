print("ReferenceNumber, Score");

db.loans.find({TenantId: "rockloans"}, { ReferenceNumber: true, "Scores.Name": true, "Scores.InitialScore": true }).forEach(function(loan){
    if (!loan.Scores || loan.Scores.length == 0) {
        print(loan.ReferenceNumber + ",");
    }
    else {
        print(loan.ReferenceNumber + "," + loan.Scores[0].InitialScore);
    }
});
