function printDate(d) { return d ? d.DateTime.toISOString() + ' ' + (d.Offset/60) : ''; }
db.getCollection('schedule').find({}).forEach(function(s){
    try {
        var loan = db.loans.findOne({ReferenceNumber: s.LoanReferenceNumber});
        s.Installments.forEach(function(i){
            if (i.AnniversaryDate && i.AnniversaryDate.DateTime.getUTCDate() !== loan.Terms.OriginationDate.DateTime.getUTCDate())
                print(loan.ReferenceNumber + ' -- ' + loan.Terms.OriginationDate.DateTime.toISOString() + ' -- ' + i.AnniversaryDate.DateTime.toISOString());
        });
    }
    catch (e){print('ERROR', e);}
});
