const util = require('util');
const abort = require('./abort.js');

function CommandQueue () {
  this.queue = [];
  return this;
};

CommandQueue.prototype.add = function run(command) {
  this.queue.push(command);
  return this;
}

CommandQueue.prototype.run = function run(options) {
  const interval = options && options.interval ? parseInt(options.interval) : 0;
  const queue = this.queue;
  let index = 0;
  function runNextCommand() {
    const command = queue[index++];
    if (command) {
      let promise;
      try {
        promise = command();
      }
      catch(e) {
        console.log('error executing command #%d:', index, e);
      }
      if (promise) {
        if(interval > 0) {
          promise.then(delay(interval).then(runNextCommand).catch(onError));
          promise.catch(onError);
        }
        else {
          promise.then(runNextCommand);
          promise.catch(onError);
        }
      }
    }
  };
  function delay(interval) {
    return new Promise((reject, resolve) => {
      setTimeout(resolve, interval);
    });
  }
  function onError() {
    abort(util.format('command #%d failed: ', index, JSON.stringify(arguments)));
  }
  runNextCommand();
}

module.exports = CommandQueue;
