const fs = require('fs');
const stripJsonComments = require('strip-json-comments');

function parseJsonFile(file) {
  let config;
  try {
    config = JSON.parse(stripJsonComments(fs.readFileSync(file, 'utf8')));
  }
  catch (e) {
    console.log('failed to parse json file at %s: %s', file, e);
    process.exit(1);
  }
  if (!config) {
    console.log('failed to parse json file at %s', file);
    process.exit(1);
  }
  return config;
}

module.exports = parseJsonFile;
