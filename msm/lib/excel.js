var fs = require('fs');
var path = require('path');
var exec = require('./exec.js');
var which = require('which');

var possiblePathsToExcel = [
  'C:\\Program Files\\Microsoft Office\\Office14\\EXCEL.EXE',
  'C:\\Program Files\\Microsoft Office 15\\root\\office15\\EXCEL.EXE',
  'C:\\Program Files (x86)\\Microsoft Office\\root\\Office16\\EXCEL.EXE',
  '/Applications/Microsoft Excel.app/Contents/MacOS/Microsoft Excel'
];

function exists(filepath) {
  try { return fs.statSync(filepath).isFile(); }
  catch (err) { return false; }
}

function findExcelExecutable() {
  var excel = possiblePathsToExcel.find(exists);
  return excel ? Promise.resolve(excel) : findExcelInSystemPath();
}

function findExcelInSystemPath() {
  return new Promise((reject, resolve) =>
    which('EXCEL.EXE', (err, excel) => {
      if(err) reject(err);
      else resolve(excel);
    })
  );
}

module.exports = {
  open: function open(filepath) {
    if (!exists(filepath))
      return Promise.reject('file does not exist: ' + filepath);

    return findExcelExecutable().then(function(excel) {
      return exec(excel, [path.resolve(filepath)], {detached: true});
    });
  }
};
