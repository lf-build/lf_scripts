const child_process = require('child_process');

module.exports = function exec(cmd, args, options, output) {
  return new Promise((reject, resolve) => {
    const spawn = child_process.spawn;
    const child = spawn(cmd, args, options || {});

    output = output || function(buffer) { console.log(buffer.toString()); };

    child.stdout.on('data', function (buffer) {
      output(buffer);
    });

    child.stderr.on('data', function (buffer) {
      console.log(buffer.toString());
    });

    child.on('close', function(code) {
      if (code === 0)
        resolve(code);
      else
        reject(new Error(`command '${cmd}' exited with code ${code}`));
    });
  });
}
