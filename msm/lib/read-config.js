const fs = require('fs');
const path = require('path');
const loadJson = require('./load-json.js');

function getUserHome() {
  return process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'];
}

function fileExists(filePath) {
  try { return fs.statSync(filePath).isFile(); }
  catch (err) { return false; }
}

module.exports = function(configName) {
  const configDir = path.join(getUserHome(), '.lf');
  const configFile = path.join(configDir, configName + '.json');
  if (!fileExists(configFile)) {
    console.log('config file not found at %s', configFile);
    process.exit(1);
  }
  return loadJson(configFile);
}
