const abort = require('./lib/abort');
const assert = require('assert');
const exec = require('./lib/exec');
const format = require('util').format;
const fs = require('fs');
const moment = require('moment-timezone');
const newline = require('os').EOL;
const path = require('path');
const readConfig = require('./lib/read-config');
const CommandQueue = require('./lib/command-queue');
const MongoClient = require('mongodb').MongoClient;
const Excel = require('exceljs');
const ExcelProgram = require('./lib/excel');

run();

function run() {
  const args = process.argv.slice(2);
  const command = args[0];
  const configName = args[1];
  const refnum = args[2];

  if (args.length !== 3 || (command !== 'start' && command !== 'finish') || isBlank(configName) || isBlank(refnum)) {
    abort('Usage: msm start|finish <config> <refnum>');
  }
  else {
    const config = validateConfig(readConfig(configName));
    const root = createRootDir();

    console.log('tenant: %s', config.tenantId);
    console.log('timezone: %s', config.timezone);

    if (command === 'start')
      start(refnum, config, root);
    else
      finish(refnum, config, root);
  }
}

function validateConfig(config) {
  if (config == null) throw new Error('config is null or undefined');
  if (isBlank(config.tenantId)) throw new Error('config.tenantId is blank');
  if (isBlank(config.timezone)) throw new Error('config.timezone is blank');
  if (config.db == null) throw new Error('config.db is null or undefined');
  if (isBlank(config.db.url)) throw new Error('config.db.url is blank');
  return config;
}

function createRootDir() {
  const parent = 'output';
  const child = path.join(parent, moment().format('YYYY-MM-DD'));
  createDir(parent);
  createDir(child);
  return child;
}

// ~~~ START ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function start(refnum, config, root) {
  if (containsFile(`${refnum}-.*`, root))
    throw new Error(`existing files found for loan ${refnum} under ${root}`);

  fetchData(refnum, config)
    .then(data => backupSchedule(data, root))
    .then(backup => {
      console.log('json: %s', backup.jsonFile);
      console.log('mongo: %s', backup.mongoFile);
      return backup.data;
    })
    .then(data => saveScheduleAsCsv(data, root))
    .then(csv => {
      console.log('csv: %s', csv.file);
      return saveAsXlsx(csv.file, csv.data, root);
    })
    .then(xlsxFile => {
      console.log('xlsx: %s', xlsxFile);
      ExcelProgram.open(xlsxFile);
    });
}

function fetchData(refnum, config, root) {
  const url = format(config.db.url, 'loans');
  let db, loan, schedule;
  
  return MongoClient.connect(url)
    .then(result => db = result)
    .then(() => db.collection('loans').findOne({TenantId: config.tenantId, ReferenceNumber: refnum}))
    .then(result => throwErrorIfNull(loan = result, `loan not found: ${refnum}`))
    .then(() => db.collection('schedule').findOne({TenantId: config.tenantId, LoanReferenceNumber: refnum}))
    .then(result => throwErrorIfNull(schedule = result, `schedule not found: ${refnum}`))
    .then(() => db.close())
    .then(() => { return {loan, schedule}; })
    .catch(err => { db.close(); throw err; });
}

function backupSchedule(data, root) {
  const jsonFile = path.join(root, `${data.loan.ReferenceNumber}-schedule.json`);
  const mongoFile = path.join(root, `${data.loan.ReferenceNumber}-schedule-mongo.json`);
  
  if (fileExists(jsonFile))
    throw new Error(`file already exists: ${jsonFile}`);
  
  if (fileExists(mongoFile))
    throw new Error(`file already exists: ${mongoFile}`);

  const json = JSON.stringify(data.schedule, null, 2);

  fs.writeFileSync(jsonFile, json);
  fs.writeFileSync(mongoFile, toMongoDocument(json));

  return Promise.resolve({data, jsonFile, mongoFile});
}

function saveScheduleAsCsv(data, root) {
  const csv = toCsv(data.schedule); 
  const file = path.join(root, `${data.loan.ReferenceNumber}-schedule.csv`);
  fs.appendFileSync(file, csv);
  return {file, data};
}

function saveAsXlsx(csvFile, data, root) {
  const file = path.join(root, `${data.loan.ReferenceNumber}-schedule.xlsx`);
  const workbook = new Excel.Workbook();
  const options = { dateFormats: ['M/D/YYYY'] };
  
  return workbook.csv.readFile(csvFile, options)
    .then(() => workbook.csv.readFile(csvFile, options))
    .then(() => {
      const before = workbook.getWorksheet(1);
      const after = workbook.getWorksheet(2);

      before.name = 'before';
      after.name = 'after';

      before.getCell('N1').value = 'IMPORTANT: do not modify this sheet. Use the "after" sheet to modify the schedule.';

      after.getCell('N1').value = 'Rate';
      after.getCell('O1').value = 'Monthly';
      after.getCell('P1').value = 'Daily';
      after.getCell('R1').value = 'Origination';
      after.getCell('S1').value = 'Term';

      after.getCell('N2').value = { formula: `${data.loan.Terms.Rate} / 100` };
      after.getCell('O2').value = { formula: 'N2 / 12' };
      after.getCell('P2').value = { formula: 'N2 / 360' };
      after.getCell('R2').value = data.loan.Terms.OriginationDate.DateTime;
      after.getCell('S2').value = data.loan.Terms.Term;

      after.getCell('N4').value = 'IMPORTANT: always keep column M clean';

      formatNumberCells(before);
      formatNumberCells(after);

      return workbook.xlsx.writeFile(file);
    })
    .then(() => file);

  function formatNumberCells(sheet) {
    forEachRowIn(sheet, (installment, cells) => {
      cells.forEach(cell => cell.numFmt = cell.type === Excel.ValueType.Number ? '0.00' : null);
    });
  }
}

function toCsv(schedule) {
  let csv = format('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s' + newline,
    'AnniversaryDate',
    'DueDate',
    'PaymentDate',
    'PaymentId',
    'OpeningBalance',
    'PaymentAmount',
    'Interest',
    'Principal',
    'EndingBalance',
    'Status',
    'Type',
    'PaymentMethod'
  );
  
  schedule.Installments.forEach(function(installment) {
    csv += format('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s' + newline,
      formatDate(installment.AnniversaryDate),
      formatDate(installment.DueDate),
      formatDate(installment.PaymentDate),
      installment.PaymentId || '',
      installment.OpeningBalance,
      installment.PaymentAmount,
      installment.Interest,
      installment.Principal,
      installment.EndingBalance,
      InstallmentStatus.value(installment.Status),
      InstallmentType.value(installment.Type),
      PaymentMethod.value(installment.PaymentMethod)
    );
  });

  return csv;
}

// ~~~ FINISH ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function finish(refnum, config, root) {
  const jsonFile = path.join(root, `${refnum}-schedule.json`);
  const xlsxFile = path.join(root, `${refnum}-schedule.xlsx`);

  if (!fileExists(jsonFile)) throw new Error('file not found: ' + jsonFile);
  if (!fileExists(xlsxFile)) throw new Error('file not found: ' + xlsxFile);

  const schedule = JSON.parse(fs.readFileSync(jsonFile, 'utf8'));
  const workbook = new Excel.Workbook();

  schedule.Installments = [];

  workbook.xlsx.readFile(xlsxFile)
    .then(() => {
      const sheet = workbook.getWorksheet('after');

      forEachRowIn(sheet, installment => {
        schedule.Installments.push(Object.assign({}, installment, {
          AnniversaryDate: toMongoDate(installment.AnniversaryDate, config.timezone),
          DueDate: toMongoDate(installment.DueDate, config.timezone),
          PaymentDate: toMongoDate(installment.PaymentDate, config.timezone),
          PaymentMethod: parseInt(PaymentMethod.key(installment.PaymentMethod)),
          Type: parseInt(InstallmentType.key(installment.Type)),
          Status: parseInt(InstallmentStatus.key(installment.Status))
        }));
      });

      const json = JSON.stringify(schedule, null, 2);
      const fixedJsonFile = path.join(root, `${refnum}-schedule-fixed.json`);
      const fixedMongoFile = path.join(root, `${refnum}-schedule-fixed-mongo.json`);
      
      fs.writeFileSync(fixedJsonFile, json);
      console.log('schedule written to %s', fixedJsonFile);
      
      fs.writeFileSync(fixedMongoFile, toMongoDocument(json));
      console.log('schedule written to %s', fixedMongoFile);
    });
}

function toMongoDate(d, tz) {
  if (isBlank(d))
    return null;

  if (!(d instanceof Date))
    throw new Error('not a date: ' + d);

  const date = moment.tz(d.toISOString().substr(0, 10), tz);
  const offset = date.utcOffset();
  const unixTimestamp = date.unix() + offset * 60;

  return {
    DateTime: date.toISOString(),
    Ticks: unixTimestampToTicks(unixTimestamp).toString(),
    Offset: offset
  }
}

function unixTimestampToTicks(unixTimestamp) {
  const unixYear0 = 621355968000000000;
  const ticksPerSecond = 10000000;
  return unixYear0 + unixTimestamp * ticksPerSecond;
}

function forEachRowIn(sheet, fn) {
  const columns = getColumns(sheet);
  let rowIndex = 2;
  while(!isRowEmpty(rowIndex, sheet, columns)) {
    const props = {};
    const cells = [];
    columns.forEach(column => {
      const cell = sheet.getCell(`${column.letter}${rowIndex}`);
      const value = cell.value;
      props[column.name] = value === null ? null : (value.result === undefined ? value : value.result);
      cells.push(cell);
    });
    fn(props, cells);
    rowIndex++;
  }
}

function getColumns(sheet) {
  const columns = [];
  let letter = 'A';
  while (true) {
    const value = sheet.getCell(`${letter}1`).value;
    if (isBlank(value))
      break;
    columns.push({letter, name: value});
    letter = String.fromCharCode(letter.charCodeAt(0) + 1);
  }
  return columns;
}

function isRowEmpty(rowIndex, sheet, columns) {
  return columns.every(column => isBlank(sheet.getCell(`${column.letter}${rowIndex}`).value));
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function toMongoDocument(json) {
  return json
    .replace(/"_id"\s*:\s*"([\w\d]+)"/g, (match, id) => `"_id": ObjectId("${id}")`)
    .replace(/"\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{1,3})?Z"/g, (match) => `ISODate(${match})`)
    .replace(/"Ticks"\s*:\s*"(\d+)"/g, (match, ticks) => `"Ticks": NumberLong(${ticks})`);
}

function biDiMap(keyToValue) {
  const valueToKey = {};
  Object.keys(keyToValue).forEach(key => valueToKey[keyToValue[key]] = key);
  return {
    value: key => keyToValue[key],
    key: value => valueToKey[value]
  };
}

const InstallmentStatus = biDiMap({
  0: 'Scheduled',
  1: 'Pending',
  2: 'Completed'
});

const InstallmentType = biDiMap({
  0: 'Scheduled',
  1: 'Additional',
  2: 'Payoff',
  3: 'ChargeOff'
});

const PaymentMethod = biDiMap({
  0: 'Undefined',
  1: 'ACH',
  2: 'Check',
  3: 'NA'
});

function createDir(dir) {
  if (!fs.existsSync(dir))
    fs.mkdirSync(dir);
}

function fileExists(filePath) {
    try { return fs.statSync(filePath).isFile(); }
    catch (err) { return false; }
}

function containsFile(regex, dir) {
  const files = fs.readdirSync(dir);
  const found = files.find(file => {
    return file.match(regex);
  });
  return found != null;
}

function isBlank(s) {
    return s == null || s.toString().trim().length === 0;
}

function formatDate(d) {
  return d == null ? '' : moment(d.DateTime).utcOffset(d.Offset).format('M/D/YYYY');
}

function throwErrorIfNull(value, errorMsg) {
  if (value == null)
    throw new Error(errorMsg);
  return value;
}
