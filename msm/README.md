# MSM

Stands for "manual schedule modification". It helps you modify an existing payment schedule using MS Excel.

## Getting Started

Run in this directory:

```
npm install
```

That's it.

## How It Works

The process consists of two steps:

1) Start: copy and modify the schedule
2) Finish: write the final mongodb document for updating the database

### Start

```
node msm start <config> <refnum>
```

Where `config` is the name of the configuration file located at `$HOME/.lf`. There's a sample config file in this repository. Just copy it to `$HOME/.lf` and modify it according to your needs.

And `refnum` is the loan reference number.

What that command does:

* Creates the output directory based on the current date: `output/yyyy-mm-dd`
* Copies the payment schedule document from mongodb into a json file
* Converts the json file to csv
* Converts the csv file to xlsx
* Opens the xlsx with MS Excel

The xlsx file contains two sheets named `before` and `after` respectively. Modify the `after` sheet. Do not make any changes to the `before` sheet as it should serve as a reference for changes made in `after`.

### Finish

```
node msm finish <config> <refnum>
```

What it does:

* Reads the payment schedule from the `after` sheet and converts it to json format into a json file.
* Converts the json file into a mongodb document.

It does *not* modify the payment schedule in the database. That must be a manual procedure.

After performing all this process you'll end up with the following files:

```
output/
└── 2016-12-14/
    ├── 111622-schedule-fixed-mongo.json
    ├── 111622-schedule-fixed.json
    ├── 111622-schedule-mongo.json
    ├── 111622-schedule.csv
    ├── 111622-schedule.json
    └── 111622-schedule.xlsx
```

Use the `xxxxxx-schedule-fixed-mongo.json` to manually update the database. Note that it keeps the original files for backup purposes in case you need to restore the payment schedule.
